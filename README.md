# EasyGeoing \[ē-zē ˈgō-iŋ\]#

Searching your neighbourhood made easy! Given a location in longitude and 
latitude, EasyGeoing provides search bounds for a database search and a filter
to filter the result. No specialized database support for location search is
required.

## Usage ##

The code block below shows a complete search run using a JPA query (Hibernate et. al.).
It assumes a PlaceItem entity class with `longitude` and `latitude` properies.
The JPA query will produce a preliminary result ('all points within a square'),
then the result is converted to a Java 8 stream by checking the distance from
the user's location. Finally, we sort by distance. See Javadoc for details.

``` java
// create a DistanceComputation with user's location and search radius in meters
DistanceComputation<PlaceItem> dc = new DistanceComputation<>(referenceLatitude, referenceLongitude, 100);

// create query bounds that we need for the JPA search
QueryBounds qb = dc.queryBounds();

// run JPA query to get raw list of items. This list
// will still contain false positives
List<PlaceItem> rawItems = entityManager.createQuery(
    "SELECT p FROM PlaceItem p " +
    "WHERE p.longitude BETWEEN :longitudeMin AND :longitudeMax " +
    "  AND p.latitude  BETWEEN :latitudeMin AND :latitudeMax ",
    PlaceItem.class
)
    .setParameter("longitudeMax", qb.longitudeMax())
    .setParameter("longitudeMin", qb.longitudeMin())
    .setParameter("latitudeMax", qb.latitudeMax())
    .setParameter("latitudeMin", qb.latitudeMin())
    .getResultList();

// filter raw list of PlaceItems to remove false positives,
// producing the final result. This also allows to sort
// by distance
List<PlaceItem> result = rawItems.stream()
    .stream()    // Java 8 stream: map each result to DistanceOperand
    .map(item -> dc.distanceOperandOf(item, item.getLatitude(), item.getLongitude())
    .filter(DistanceOperand::isWithinRadius)    // filter results that are within range
    .sort(DistanceOperand::getDistance)         // sort by distance
    .map(DistanceOperand::getItem)              // map back to stream of PlaceItems
    .collect(Collectors.toList());              // DONE!
```

## Maven Depencency ##
``` xml
<depencency>
    <groupId>org.bitbucket.easygeoing</groupId>
    <artifactId>easygeoing</artifactId>
    <version>1.0.0</version>
</dependency>
```