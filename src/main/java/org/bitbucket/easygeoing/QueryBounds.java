/*
 * Copyright 2017 The EasyGeoing Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.bitbucket.easygeoing;

/**
 * Query bounds to be used in a database query to find a constrained
 * superset of the actual items within the search radius. A database
 * search of items with positions in itemLat and itemLon will look
 * like this:
 *
 * <pre>{@code
 * SELECT * FROM items
 * WHERE itemLat BETWEEN :latitudeMin AND :latitudeMax
 *   AND itemLon BETWEEN :longitudeMin AND :longitudeMax;
 * }</pre>
 *
 * <p>...where {@code :longitudeMin}, {@code :longitudeMax}, {@code :latitudeMin}
 * and {@code :latitudeMax} are the boundaries provided by the getters in
 * this object. Make sure that their unit matches the unit stored in the
 * database ({@link #getUnit()} provides the unit, for reference.</p>
 *
 * A QueryBounds instance is provided by {@link DistanceComputation#queryBounds()}
 */
public class QueryBounds {

	private final AngleUnit unit;
	private final double latitudeMin;
	private final double latitudeMax;
	private final double longitudeMin;
	private final double longitudeMax;

	QueryBounds(AngleUnit unit, double latMin, double latMax, double lonMin, double lonMax) {
		this.unit = unit;
		this.latitudeMin = latMin;
		this.latitudeMax = latMax;
		this.longitudeMin = lonMin;
		this.longitudeMax = lonMax;
	}

	/**
	 * @return the angular unit in which this bound's values are given.
	 */
	public AngleUnit getUnit() {
		return unit;
	}

	/**
	 * @return latitude, lower bound
	 */
	public double latitudeMin() {
		return latitudeMin;
	}

	/**
	 * @return latitude, upper bound
	 */
	public double latitudeMax() {
		return latitudeMax;
	}

	/**
	 * @return longitude, lower bound
	 */
	public double longitudeMin() {
		return longitudeMin;
	}

	/**
	 * @return longitude, upper bound
	 */
	public double longitudeMax() {
		return longitudeMax;
	}
	
}
