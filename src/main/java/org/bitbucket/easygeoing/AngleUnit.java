/*
 * Copyright 2017 The EasyGeoing Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.bitbucket.easygeoing;

/**
 * Describes different units for measuring angles. Provides conversion 
 * functionality via {@link #convert(double, org.bitbucket.easygeoing.AngleUnit) }.
 * @author upachler
 */
public enum AngleUnit {
	DEGREE,
	RADIAN;

	/**
	 * Converts angles in the unit represented by this instance to the 
	 * {@code targetUnit}. For instance, {@code RADIANS.convert(Math.PI, DEGREES)}
	 * will return 180. Note that conversions will wrap to the equivalent 
	 * of the interval ]-180,180] degrees.
	 * @param sourceValue	angle value, in the unit represented by this instance
	 * @param targetUnit	the target unit to convert the source value to
	 * @return {@code} sourceValue converted to the target Unit
	 */
	public double convert(double sourceValue, AngleUnit targetUnit) {
		if(this == targetUnit) {
			return sourceValue; // target == source
		}
		
		if(targetUnit == DEGREE) {
			assert this == RADIAN;
			return rad2deg(sourceValue);
		} else {
			assert targetUnit == RADIAN;
			assert this == DEGREE;
			return deg2rad(sourceValue);
		}
	}

	public static double deg2rad(double deg) {
		// normalize within ]-180,+180]
		if(deg <= -180d) {
			deg += 360d;
		}
		if(deg > 180d) {
			deg -= 360d;
		}
		return deg / 180d * Math.PI;
	}
	
	public static double rad2deg(double rad) {
		// normalize within [-π,+π[
		if(rad <= -Math.PI) {
			rad += 2*Math.PI;
		}
		if(rad > Math.PI) {
			rad -= 2*Math.PI;
		}
		return rad / Math.PI * 180d;
	}
	
}

