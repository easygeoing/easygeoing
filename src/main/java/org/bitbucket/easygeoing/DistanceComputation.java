/*
 * Copyright 2017 The EasyGeoing Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.bitbucket.easygeoing;

import static java.lang.Math.*;
import static org.bitbucket.easygeoing.AngleUnit.*;

/**
 * A DistanceComputation performs distance queries between a single reference
 * location and multiple target locations. For example, if he reference location 
 * is the user's current location, the target locations could be tobacco shops.
 * 
 * <p>For a given radius (range) and reference coordinates, the class supports the 
 * following operations on a dataset of target locations, ideally performed in this
 * order:</p>
 * 
 * <ul>
 * <li>Check if target location is within the radius of the reference location</li>
 * <li>Calculate the distance (uses the result from the first operation)</li>
 * </ul>
 * 
 * <h2>Usage</h2>
 * First, create a DistanceComputation object, based on some location with
 * longitude/latitude:
 * 
 * <pre>{@code 
 *     DistanceComputation<Item> dc = new DistanceComputation<>(referenceLatitude, referenceLongitude, 100);
 * }</pre>
 * 
 * <p>Then, use {@link QueryBounds} to search the database for items that
 * are inside the quadrilateral surrounding the search radius. The example 
 * shows a JPA query, but plain SQL would look very similar. {@code MyPlace}
 * is assumed to have {@code latitude} and {@code longitude} properties.</p>
 * 
 * <pre>{@code 
 *     QueryBounds qb = dc.queryBounds();
 *     
 *     List<PlaceItem> items = entityManager.createQuery(
 *             "SELECT p FROM PlaceItem p " +
 *             "WHERE p.longitude BETWEEN :longitudeMin AND :longitudeMax " +
 *             "  AND p.latitude  BETWEEN :latitudeMin AND :latitudeMax ",
 *             PlaceItem.class
 *         )
 *         .setParameter("longitudeMax", qb.longitudeMax())
 *         .setParameter("longitudeMin", qb.longitudeMin())
 *         .setParameter("latitudeMax", qb.latitudeMax())
 *         .setParameter("latitudeMin", qb.latitudeMin())
 *         .getResultList();
 * }</pre>
 * 
 * <p>The result still contains false positives, which are in within the
 * quadrilateral defined by the {@link QueryBounds}, but not within the search
 * radius. So we need to filter the result, like this:</p>
 * 
 * <pre>{@code
 *     List<DistanceOperand> distancesWithinRange = items.stream()
 *         .map(item -> dc.distanceOperandOf(item, item.getLatitude(), item.getLongitude())
 *         .filter(DistanceOperand::isWithinRadius)
 *         .sort(DistanceOperand::getDistance)
 *         .collect(Collectors.toList());
 * }</pre>
 * 
 * <p>The result contains DistanceOperands of all items that were within the given 
 * range, sorted by distance. The operand provides the item with {@link DistanceOperand#getItem()}.</p>
 * 
 * <h2>The Mathematics</h2>
 * 
 * <p>In order to simply using this class, reading this section is not required.
 * However, if you need to make changes to this class, reading is mandatory.</p>
 * 
 * <p>The original distance function is defined below, where {@code (λ_a,ϕ_a)} are the 
 * coordinates of the reference point (our own location), and {@code (λ_b,ϕ_b)}
		are the coordinates of the target location that we're currently calculating
		the distance to. {@code R} is the Earth's radius (as per the WGS 84 model). 
		Because we'll be checking multiple target locations,
		it makes sense to optimize the distance calculation by separating
		it into terms that we can pre-calculate and terms that we'll be
		calculating anew for each target location.</p>
		
		<p>We'll start with the original formula calculating the distance d:</p>
		
		<p>i) {@code Δϕ  =  ϕ_b-ϕ_a}</p>
		
		<p>ii) {@code Δλ  =  λ_b-λ_a}</p>
		
		<p>iii) {@code d  =  2*R * asin( sqrt( sq(sin(Δϕ/2)) + cos(ϕ_a) * cos(ϕ_b) * sq(sin(Δλ/2)) ) )}</p>
		
		<p>If we want to make sure the calculated distance d is less than
		a reference distance d_r, the following inequality must hold:</p>
		
		<p>iv) {@code d_r  >  2*R * asin( sqrt( sq(sin(Δϕ/2)) + cos(ϕ_a) * cos(ϕ_b) * sq(sin(Δλ/2)) ) )}</p>
		
		<p>To minimize computational complexity, we transform to get all constants
		to one side.</p>
		
		<p>v) {@code d_r/(2*R)  >  asin( sqrt( sq(sin(Δϕ/2)) + cos(ϕ_a) * cos(ϕ_b) * sq(sin(Δλ/2)) ) )}</p>
		
		<p>vi) {@code sin(d_r/(2*R))  >  sqrt( sq(sin(Δϕ/2)) + cos(ϕ_a) * cos(ϕ_b) * sq(sin(Δλ/2)) )}</p>
		
		<p>vii) {@code sq(sin(d_r/(2*R)))  >  sq(sin(Δϕ/2)) + cos(ϕ_a) * cos(ϕ_b) * sq(sin(Δλ/2))}</p>
		
		<p>We call the left hand side distance reference distance measure m_r, and
		the right hand side actual distance measure m_t:</p>
		
		<p>viii) {@code m_r  =  sq(sin(d_r/(2*R)))}</p>
		
		<p>ix) {@code m_t  =  sq(sin(Δϕ/2)) + cos(ϕ_a) * cos(ϕ_b) * sq(sin(Δλ/2))}</p>
		
		<p>Therefore, the inequation (iv) can expressed as:</p>
		
		<p>x) {@code m_r  >  m_t}</p>
		
		<p>This allows us to pre-calculate {@code m_r} once, because it is a constant term
		over all target locations.
		{@code m_t} changes for each location that we're testing against (each has
		its own value of {@code (ϕ_b, λ_b)}).</p>
		
		<p>Because {@code m_t} is a term of (iii), we can write (iii) as:</p>
		
		<p>xi) {@code d  =  2*R * asin( sqrt( m_t ) )}</p>
		
		<p>This is convenient, because after checking whether the target location
		is within the reference distance by employing inequation (x), then (and
		only then) we'll calcutate the actual distance d with equation (xi).</p>
		
 * @author upachler
 */
public class DistanceComputation<T> {

	// earth's radius, according to WGS84 and Wikipedia:
	// https://en.wikipedia.org/wiki/World_Geodetic_System
	public static final double R = 6378137;
	
	private final AngleUnit unit;
	private final double ϕ_a;
	private final double λ_a;
	private final double d_r;
	private final double m_r;
	private QueryBounds qb;
	
	/**
	 * Convenience constructor defaulting to DEGREE as angle unit, same as calling 
	 * {@code DistanceComputation(DEGREE, referenceLatitude, referenceLongitude, radius)}. 
	 * @param referenceLatitude	latitude of the center point around which we're searching
	 * @param referenceLongitude	longitude of the center point around which we're searching
	 * @param radius	the search radius
	 */
	public DistanceComputation(double referenceLatitude, double referenceLongitude, double radius) {
		this(DEGREE, referenceLatitude, referenceLongitude, radius);
	}
	
	/**
	 * Creates a new {@code DistanceComputation} instance, using the provided
	 * angle unit. All angles provided to or by this instance are in that unit
	 * (so everything is either in DEGREE or in RADIAN). 
	 * @param unit	the unit to use for describing angles (and therefore geographical
	 *	coordinates like latitude or longitude)
	 * @param referenceLatitude	latitude of the center point around which we're searching
	 * @param referenceLongitude	longitude of the center point around which we're searching
	 * @param radius	the search radius
	 */
	public DistanceComputation(AngleUnit unit, double referenceLatitude, double referenceLongitude, double radius) {
		this.unit = unit;
		this.ϕ_a = unit.convert(referenceLatitude, AngleUnit.RADIAN);
		this.λ_a = unit.convert(referenceLongitude, AngleUnit.RADIAN);
		this.d_r = radius;
		this.m_r = calculate_m_r(d_r);
	}

	/**
	 * @return	The angle unit using in the instance calculation (as provided to the
	 * constructor)
	 */
	public AngleUnit getUnit() {
		return unit;
	}
	
	public class DistanceOperandImpl implements DistanceOperand<T> {

		private final T item;
		private final double ϕ_b;
		private final double λ_b;
		
		private double m_t;
		private boolean m_t_calculated;
		private double d;
		private boolean d_calculated;
		
		private DistanceOperandImpl(T item, double ϕ_b, double λ_b) {
			this.item = item;
			this.ϕ_b = ϕ_b;
			this.λ_b = λ_b;
		}
		
		private double get_m_t() {
			if(!m_t_calculated) {
				m_t = calculate_m_t(ϕ_a, λ_a, ϕ_b, λ_b);
				m_t_calculated = true;
			}
			return m_t;
		}
		
		@Override
		public boolean isWithinRange() {
			return get_m_t() < m_r;
		}
		
		@Override
		public double getDistance() {
			if(!d_calculated) {
				d = calculate_d(get_m_t());
				d_calculated = true;
			}
			return d;
		}
		
		@Override
		public T getItem() {
			return item;
		}
	}
	
	static double referenceLatMinMax(double ϕ_a, double d, double cos_θ) {
		
		// reference function, as provided in 
		// https://zaemis.blogspot.de/2011/01/geolocation-search.html
		// for calculating search bounds. We use this function only for testing
		// and as a reference for the more optimized implementation; 
		
		// Because we only calculate min and max for the search, we provide
		// cos_θ directly, because it can only ever be -1 or +1 (for calculating
		// the minimum latitude or the maximum latitude, respectively)
		
		double ϕ_b = asin( sin(ϕ_a) * cos(d/R) + cos(ϕ_a) * sin(d/R) * cos_θ );
		
		return ϕ_b;
	}
	
	static double referenceLonMinMax(double λ_a, double ϕ_a, double d, double sin_θ) {
		
		// reference function, as provided in 
		// https://zaemis.blogspot.de/2011/01/geolocation-search.html
		// for calculating search bounds. We use this function only for testing
		// and as a reference for the more optimized implementation; 
		
		// Because we only calculate min and max for the search, we provide
		// sin_θ directly, because it can only ever be -1 or +1 (for calculating
		// the minimum latitude or the maximum latitude, respectively)
		
		// Also note that this function calls referenceLatMinMax() to calculate
		// λ_b. On closer inspection we see that the call is unnecessary, as
		// certain terms can be resolved to a much simpler equation to create
		// the same result. However, this is not the purpose of the reference
		// functions, but of the actual implementation used in production.
		double ϕ_b = referenceLatMinMax(ϕ_a, d, 0);
		double λ_b = λ_a + atan2(sin_θ * sin(d/R) * cos(ϕ_a), cos(d/R) - sin(ϕ_a) * sin(ϕ_b));
		
		return λ_b;
	}
	
	
	/**
	 * Provides latitude and longitude bounds for a database query to search
	 * for items in a database table. The respective query result will need
	 * to be filtered later to eliminate false positives. Query bounds are
	 * calculated on demand, calling this method multiple times will 
	 * only calculate the bounds on the first invocation. Subsequent calls
	 * will use a stored result.
	 * @return query bounds to use for a database search.
	 */
	public QueryBounds queryBounds() {
		if(qb == null) {
			qb = calculateQueryBounds();
		}
		return qb;
	}
	
	private QueryBounds calculateQueryBounds() {
		double d = d_r;
		// refined formula (eliminating ϕ_b in the equation for λ_b:
		// ϕ_b = asin( sin(ϕ_a) * cos(d/R) + cos(ϕ_a) * sin(d/R) * cos_θ );
		// λ_b = λ_a + atan2(sin_θ * sin(d/R) * cos(ϕ_a), cos(d/R) - sq(sin(ϕ_a)) * cos(d/R));
		// 
		// ϕ_b calculated for min and max (using -1 and +1 for cos_θ, respectively)
		// ϕ_b_min = asin(f_ϕ_center - f_ϕ_offset)
		// ϕ_b_max = asin(f_ϕ_center + f_ϕ_offset)
		// f_ϕ_center = sin(ϕ_a) * cos(d/R) 
		// f_ϕ_offset = cos(ϕ_a) * sin(d/R)
		//
		// λ_b = calculated for min and max (using -1 and +1 for sin_θ, respectively)
		// λ_b = λ_a + atan2(sin_θ * cos(ϕ_a) * sin(d/R), cos(d/R) - sq(sin(ϕ_a)) * cos(d/R));
		// λ_b = λ_a + atan2(sin_θ * cos(ϕ_a) * sin(d/R), cos(d/R) * (1 - sq(sin(ϕ_a))));
		//
		// λ_b_min = λ_a + atan2( f_ϕ_offset, f_λ)
		// f_λ = cos(d/R) * (1 - sq(sin(ϕ_a)))
		//
		// There is certainly more room for optimization here, should the
		// need arise. 
		
		double f_ϕ_center = sin(ϕ_a) * cos(d/R);
		double f_ϕ_offset = cos(ϕ_a) * sin(d/R);
		double ϕ_b_min = asin(f_ϕ_center - f_ϕ_offset);
		double ϕ_b_max = asin(f_ϕ_center + f_ϕ_offset);
		
		double f_λ = cos(d/R) * (1 - sq(sin(ϕ_a)));
		double λ_b_max = λ_a + atan2( f_ϕ_offset, f_λ);
		double λ_b_min = λ_a + atan2( -f_ϕ_offset, f_λ);
		
		return new QueryBounds(
			unit, 
			RADIAN.convert(ϕ_b_min, unit), 
			RADIAN.convert(ϕ_b_max, unit),
			RADIAN.convert(λ_b_min, unit),
			RADIAN.convert(λ_b_max, unit)
		);
	}
	
	/**
	 * Provides a distance operand for the given item and its geocoordinates.
	 * Latitude/longitude are given in degrees, {@code [-180,+180[}). The
	 * operand can be used to perform distance calculations relative to the
	 * reference point.
	 * @param item	The item to associate with this operand, can be retrieved later.
	 * @param itemLatitude	the item's latitude
	 * @param itemLongitude	the item's longitude
	 * @return	a new distance operand to use for distance calculations
	 */
	public DistanceOperand<T> distanceOperandOf(T item, double itemLatitude, double itemLongitude) {
		double ϕ_b = unit.convert(itemLatitude, RADIAN);
		double λ_b = unit.convert(itemLongitude, RADIAN);
		return new DistanceOperandImpl(item, ϕ_b, λ_b);
	}
	
	private static double sq(double x) {
		return x*x;
	}
	
	/**
	 * Distance reference function. Mostly used for testing.
	 * @param centroidLatitudeA
	 * @param centroidLongitudeA
	 * @param centroidLatitudeB
	 * @param centroidLongitudeB
	 * @return 
	 */
	static double referenceDistance(double centroidLatitudeA, double centroidLongitudeA, double centroidLatitudeB, double centroidLongitudeB) {
		// distance, as defined in the blog entry
		// https://zaemis.blogspot.de/2011/01/geolocation-search.html
		// Note that the formula shown in the image there is slightly wrong, 
		// but the php code is correct (the formula has a term
		// cos(ϕ_a) * cos(ϕ_a), but the code uses cos(ϕ_a) * cos(ϕ_b)).
		double ϕ_a = deg2rad(centroidLatitudeA);
		double λ_a = deg2rad(centroidLongitudeA);
		double ϕ_b = deg2rad(centroidLatitudeB);
		double λ_b = deg2rad(centroidLongitudeB);
		
		double Δϕ = ϕ_b - ϕ_a;
		double Δλ = λ_b - λ_a;
		
		double d = 2*R * asin( sqrt( sq(sin(Δϕ/2)) + cos(ϕ_a) * cos(ϕ_b) * sq(sin(Δλ/2)) ) );
		
		return d;
	}
	
	/**
	 * calculate a distance from a distance measure (m_t)
	 * @param m_t the distance measure
	 * @return the distance in meters
	 */
	static double calculate_d(double m_t) {
		double d = 2*R * asin( sqrt( m_t ) );
		
		return d;
	}
	
	static double calculate_m_r(double d_r) {
		return sq(sin(d_r/(2*R)));
	}
	/**
	 * Calculate a distance measure from reference and target locations
	 * @param latitude
	 * @param longitude
	 * @return 
	 */
	static double calculate_m_t(double ϕ_a, double λ_a, double ϕ_b, double λ_b) {
		
		double Δϕ = ϕ_b - ϕ_a;
		double Δλ = λ_b - λ_a;
		
		double m_t = sq(sin(Δϕ/2)) + cos(ϕ_a) * cos(ϕ_b) * sq(sin(Δλ/2));
		
		return m_t;
	}
}
