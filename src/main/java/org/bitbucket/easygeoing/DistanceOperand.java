/*
 * Copyright 2017 The EasyGeoing Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.bitbucket.easygeoing;

/**
 * Allows a distance calculation from a single item representing a location
 * to the reference location (the center of our search radius).
 * This object allows to perform a two stage check: First, a range only
 * check is performed ('is item within range?'). Then, the distance
 * is calculated ('how far is it away'). This allows to save computation
 * time if the item is discarded before calculating the distance.
 * Distance operands are provided by the {@link DistanceComputation#distanceOperandOf(java.lang.Object, double, double) }
 * method.
 * 
 * Note that the first calls to {@link #isWithinRange() } and {@link #getDistance() }
 * are both computationally expensive, where the latter builds on the result
 * of the former. The results are stored, so that repeated calls will not
 * recalculate the results.
 */
public interface DistanceOperand<T> {
	
	/**
	 * @return The item that was provided when the operand was created.
	 */
	T getItem();

	/**
	 * Checks whether the provided coordinates are within the search radius
	 * @return whether the item is within range or not
	 */
	boolean isWithinRange();
	
	/**
	 * Calculates the distance to the provided coordinates.
	 * @return the distance
	 */
	double getDistance();
	
}
