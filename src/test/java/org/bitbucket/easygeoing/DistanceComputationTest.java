/*
 * Copyright 2017 The EasyGeoing Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.bitbucket.easygeoing;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.bitbucket.easygeoing.AngleUnit.*;

/**
 *
 * @author upachler
 */
public class DistanceComputationTest {
	
	final double SHELL_LAT = 48.7784876d;
	final double SHELL_LON = 9.1553874d;
	final String SHELL_ID = "Shell";
	
	final double ESSO_LAT = 48.7749551d;
	final double ESSO_LON = 9.1580862d;
	final String ESSO_ID = "Esso";

	final double JET_LAT = 51.447769165039063d;
	final double JET_LON = 7.5832319259643555d;
	final String JET_ID = "Jet";

	final double MIDDLE_LAT = (ESSO_LAT + SHELL_LAT)/2;
	final double MIDDLE_LON = (ESSO_LON + SHELL_LON)/2;
	
	final double EPSILON = 0.000001d;

	public DistanceComputationTest() {
	}
	
	@Test
	public void testReferenceDistance() {
		double d = DistanceComputation.referenceDistance(ESSO_LAT, ESSO_LON, JET_LAT, JET_LON);
		assertEquals(318000d, d, 1000d);
	}
	
	/**
	 * Test of distanceOperandOf method, of class DistanceComputation.
	 */
	@Test
	public void testDistanceOperandOf() {
		System.out.println("distanceOperandOf");
		double essoDistance = DistanceComputation.referenceDistance(MIDDLE_LAT, MIDDLE_LON, ESSO_LAT, ESSO_LON);

		DistanceComputation<String> dc;
		DistanceOperand op;
		boolean withinRange;
		double distance;
		
		dc = new DistanceComputation(MIDDLE_LAT, MIDDLE_LON, essoDistance * 0.9);
		op = dc.distanceOperandOf("Esso", ESSO_LAT, ESSO_LON);
		withinRange = op.isWithinRange();
		distance = op.getDistance();
		assertFalse(withinRange);
		assertEquals(essoDistance, distance, EPSILON);
		
		dc = new DistanceComputation(MIDDLE_LAT, MIDDLE_LON, essoDistance * 1.1);
		op = dc.distanceOperandOf("Esso", ESSO_LAT, ESSO_LON);
		withinRange = op.isWithinRange();
		distance = op.getDistance();
		assertTrue(withinRange);
		assertEquals(essoDistance, distance, EPSILON);
	}
	
	@Test
	public void testQueryBounds() {
		final double radius = 500;
		
		DistanceComputation dc = new DistanceComputation(MIDDLE_LAT, MIDDLE_LON, radius);
		
		QueryBounds qb = dc.queryBounds();
		
		double middleLatRad = DEGREE.convert(MIDDLE_LAT, RADIAN);
		double middleLonRad = DEGREE.convert(MIDDLE_LON, RADIAN);
		
		double refLatMin = DistanceComputation.referenceLatMinMax(middleLatRad, radius, -1);
		double refLatMax = DistanceComputation.referenceLatMinMax(middleLatRad, radius, +1);
		double refLonMin = DistanceComputation.referenceLonMinMax(middleLonRad, middleLatRad, radius, -1);
		double refLonMax = DistanceComputation.referenceLonMinMax(middleLonRad, middleLatRad, radius, +1);
		
		double refLatMinDeg = RADIAN.convert(refLatMin, DEGREE);
		double refLatMaxDeg = RADIAN.convert(refLatMax, DEGREE);
		double refLonMinDeg = RADIAN.convert(refLonMin, DEGREE);
		double refLonMaxDeg = RADIAN.convert(refLonMax, DEGREE);
		
		assertEquals(refLatMinDeg, qb.latitudeMin(), EPSILON);
		assertEquals(refLatMaxDeg, qb.latitudeMax(), EPSILON);
		assertEquals(refLonMinDeg, qb.longitudeMin(), EPSILON);
		assertEquals(refLonMaxDeg, qb.longitudeMax(), EPSILON);
	}
}
