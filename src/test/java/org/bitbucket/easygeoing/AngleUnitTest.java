/*
 * Copyright 2017 The EasyGeoing Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.bitbucket.easygeoing;

import static org.bitbucket.easygeoing.AngleUnit.*;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author upachler
 */
public class AngleUnitTest {
	
	/**
	 * Test of convert method, of class AngleUnit.
	 */
	@Test
	public void testConvert() {
		
		System.out.println("convert");
		
		double epsilon = 0.00000001;
		
		// round trip conversion checks
		assertEquals( 30.0, RADIAN.convert(DEGREE.convert( 30.0, RADIAN), DEGREE), epsilon);
		assertEquals( 45.0, RADIAN.convert(DEGREE.convert( 45.0, RADIAN), DEGREE), epsilon);
		assertEquals(180.0, RADIAN.convert(DEGREE.convert(180.0, RADIAN), DEGREE), epsilon);
		assertEquals(-90.0, RADIAN.convert(DEGREE.convert(-90.0, RADIAN), DEGREE), epsilon);
		assertEquals(-30.0, RADIAN.convert(DEGREE.convert(-30.0, RADIAN), DEGREE), epsilon);
		assertEquals(    0, RADIAN.convert(DEGREE.convert(  0.0, RADIAN), DEGREE), epsilon);
		
		assertEquals( 30.0, RADIAN.convert(Math.PI/6, DEGREE), epsilon);
		assertEquals( 90.0, RADIAN.convert(Math.PI/2, DEGREE), epsilon);
		assertEquals( -90.0, RADIAN.convert(-Math.PI/2, DEGREE), epsilon);
	}

	
	
}
